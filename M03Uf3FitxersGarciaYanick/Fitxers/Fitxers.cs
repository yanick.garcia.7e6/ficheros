﻿using System;

namespace Fitxers
{
    class Fitxers
    {
        static void Main(string[] args)
        {
            var menu = new Fitxers();
            menu.Menu();
        }
        public void Menu()
        {
            Console.WriteLine("1-Parell y senar");
            Console.WriteLine("2-Inventario");
            Console.WriteLine("3-Word count");
            Console.WriteLine("4-Config file language");
            Console.WriteLine("5-Find Paths");
            Console.WriteLine("0-Salir");
            Console.WriteLine("Elija una opción:");
            var opcio = Console.ReadLine();
            switch (opcio)
            {
                case "1":
                    ParellYSenar();
                    break;
                case "2":
                    Inventario();
                    break;
                case "3":
                    WordCount();
                    break;
                case "4":
                    ConfigFileLanguage();
                    break;
                case "5":
                    FindPaths();
                    break;
                case "0":
                    Salir();
                    break;
                default:
                    Console.WriteLine("Opción invalida");
                    break;
            }
        }

        private void Salir()
        {
            Console.WriteLine("¡¡Hasta la próxima!!");
        }

        private void FindPaths()
        {
            throw new NotImplementedException();
        }

        private void ConfigFileLanguage()
        {
            throw new NotImplementedException();
        }

        private void WordCount()
        {
            throw new NotImplementedException();
        }

        private void Inventario()
        {
            throw new NotImplementedException();
        }

        private void ParellYSenar()
        {
            throw new NotImplementedException();
        }
    }
}
